import * as THREE from './three.module.js';
import { Font, FontLoader } from './FontLoader.js';
import { TextGeometry } from './TextGeometry.js';

const wordListNames = [];
let wordListName = 'sanuli 5';
let startingPairs = [];
let startingPair = 0;
let words = {};

let wordList = null;
let wordTree = null;
let originalWordTree = null;
let wordTreeLeft = null;
let wordLength = 5;
let statusElem = null;
let guessElem = null;
let guessMaskElem = null;
let guessesDiv = null;
let guessesLeft = null;
let word = 'testi';
let guesses = [];
let timer = null;
let won = false;
let localValues = null;
let visualisation = false;
const currentVersion = 2;

let trunk = null;
let scene = null;
let letterMeshes = [];
let letterMeshes2 = [];
let camera = null;
let renderer = null;
let step = 0.01;
let rotationChange = 0;
let originalRotation = 0;
let originalX = 0;
let remainingWords = [];

const letters = 'aikutsoelrnpähmyvjödbgfczwxåq'.toUpperCase(); //
//"qwertyuiopåasdfghjklöäzxcvbnm"

function markGreen(str) {
    return '<mark class="green">' + str + '</mark>';
}

function markYellow(str) {
    return '<mark class="yellow">' + str + '</mark>';
}

function markWhite(str) {
    return '<mark class="white">' + str + '</mark>';
}

function markBlack(str) {
    return '<mark class="black">' + str + '</mark>';
}

function setStatus(str) {
    statusElem.innerHTML = str;
    clearTimeout(timer);
    timer = setTimeout(() => {
        statusElem.innerHTML = ' ';
    }, 10000);
}

function addWordRecursive(wordTreeRoot, word, index, wordLength) {
    if (index >= wordLength) return;
    if (index == wordLength - 1) {
        if (wordTreeRoot[word[index]]) {
            console.log('Duplicate ' + wordTreeRoot[word[index]]);
        }
        wordTreeRoot[word[index]] = word;
        return;
    }
    if (wordTreeRoot[word[index]] === undefined) {
        wordTreeRoot[word[index]] = {};
    }
    addWordRecursive(wordTreeRoot[word[index]], word, index + 1, wordLength);
}

function addWord(wordTree, word, wordLength) {
    addWordRecursive(wordTree, word, 0, wordLength);
}

function findRandomWordRecursive(tree, depth) {
    if (depth >= wordLength) return tree;
    let count = 0;
    for (let i = 0; i < letters.length; i++) {
        const letter = letters[i];
        if (tree[letter]) {
            count += 1;
        }
    }
    const random = Math.floor(Math.random() * count);
    count = 0;
    for (let i = 0; i < letters.length; i++) {
        const letter = letters[i];
        if (tree[letter]) {
            if (random == count) {
                return findRandomWordRecursive(tree[letter], depth + 1);
            }
            count += 1;
        }
    }
}

function findRandomWord(tree) {
    return findRandomWordRecursive(tree, 0);
}

function checkWordForLegalCharacters(word) {
    let broken = false;
    for (let j = 0; j < word.length; j++) {
        const letter = word[j];
        if (letters.search(letter) == -1) {
            broken = true;
            break;
        }
    }
    return !broken;
}

function makeWordTree(wordlist, wordLength) {
    const wordTree = {};
    let words = 0;
    for (let i = 0; i < wordlist.length; i++) {
        let word = wordlist[i];
        if (word.length == wordLength && checkWordForLegalCharacters(word)) {
            words++;
            addWord(wordTree, word, wordLength);
        } else {
            wordlist.splice(i, 1);
            i--;
        }
    }
    console.log('Game has ' + words + ' words');
    return wordTree;
}

function newWord() {
    won = false;
    remainingWords = [];
    wordList = words[wordListName]['wordList'];
    wordTree = words[wordListName]['wordTree'];
    originalWordTree = words[wordListName]['originalWordTree'];
    wordLength = words[wordListName]['info']['wordLength'];
    wordTreeLeft = JSON.parse(JSON.stringify(wordTree));
    guessesLeft.innerHTML =
        'Sinulla on ' +
        (6 - guesses.length) +
        ' arvausta jäljellä. Puussa on jäljellä ' +
        recursivelyCountTree(wordTreeLeft, 0) +
        ' mahdollista sanaa.';

    startingPair = Math.floor(Math.random() * (startingPairs.length - 1));
    let bestGuess = findRandomWord(wordTree);
    if (wordLength == 5 && (wordListName == 'sanuli 5 x' || wordListName == 'sanuli 5' || wordListName == 'suomi 5')) {
        console.log('Starting pair used');
        bestGuess = startingPairs[startingPair][0];
    }
    word = wordList[Math.floor(Math.random() * (wordList.length - 1))];
    console.log(word);
    guesses = [];
    updateGuessesList();
    guessElem.value = bestGuess;
    document.getElementById('bestGuesses').innerHTML = bestGuess;
    updateTree(scene, wordTreeLeft);
}

function makeGuessElement(guess, letterResults) {
    let str = '';
    for (let i = 0; i < wordLength; i++) {
        const letter = guess['guess'][i];
        if (guess['results'][i]['correct']) {
            str += markGreen(letter);
            letterResults[letter]['correct'] = true;
        } else if (guess['results'][i]['inWord']) {
            str += markYellow(letter);
            if (!letterResults[letter]['correct']) {
                letterResults[letter]['inWord'] = true;
            }
        } else {
            str += markBlack(letter);
            if (!letterResults[letter]['correct'] && !letterResults[letter]['inWord']) {
                letterResults[letter]['notInWord'] = true;
            }
        }
    }
    return str;
}

function updateGuessesList() {
    guessesDiv.innerHTML = '';
    const letterResults = {};
    for (let i = 0; i < letters.length; i++) {
        letterResults[letters[i]] = { letter: letters[i], correct: false, inWord: false, notInWord: false };
    }

    for (let i = 0; i < guesses.length; i++) {
        const guess = guesses[i];
        let guessP = document.createElement('p');
        guessP.innerHTML = makeGuessElement(guess, letterResults);
        guessesDiv.appendChild(guessP);
    }

    guessesDiv.appendChild(document.createElement('br'));
    const letterP = document.createElement('p');
    let str = '';
    for (let i = 0; i < letters.length; i++) {
        const letter = letters[i];
        if (letterResults[letter]['correct']) {
            str += markGreen(letter);
        } else if (letterResults[letter]['inWord']) {
            str += markYellow(letter);
        } else if (letterResults[letter]['notInWord']) {
            str += markBlack(letter);
        } else {
            str += markWhite(letter);
        }
    }
    letterP.innerHTML = str;
    const unusedLetters = guessesDiv.appendChild(letterP);
}

function doesWordTreeHas(wordTree, guess) {
    let wordTreeRoot = wordTree;
    for (let i = 0; i < wordLength; i++) {
        wordTreeRoot = wordTreeRoot[guess[i]];
        if (wordTreeRoot === undefined) {
            return false;
        }
    }
    return true;
}

function recursivelyCountTree(tree, index) {
    if (index >= wordLength) return 1;
    let sum = 0;

    Object.values(tree).forEach((value) => {
        sum += recursivelyCountTree(value, index + 1);
    });
    return sum;
}

function doesWordMatchMask(word, guesses, correctOne) {
    for (let j = 0; j < guesses.length; j++) {
        const guess = guesses[j]['guess'];
        const correct = j == guesses.length - 1 ? correctOne : guesses[j]['correct'];

        let missing = [];
        for (let i = 0; i < wordLength; i++) {
            if (correct[i] != 1 && guess[i] == word[i]) {
                return false;
            }
            if (correct[i] == 1) {
                if (guess[i] != word[i]) {
                    return false;
                }
            } else {
                missing.push({ letter: word[i], position: i });
            }
        }
        let missingAtStart = 0;

        const missingX = [];
        for (let i = 0; i < correct.length; i++) {
            if (correct[i] == 2) {
                missingAtStart += 1;
                missingX.push({ letter: guess[i], position: i });
            }
        }
        let found = 0;
        for (let i = 0; i < wordLength; i++) {
            if (correct[i] != 1) {
                const letter = guess[i];
                const ind = i;
                const find = (element) => element['letter'] == letter && element['position'] != ind;
                const index = missing.findIndex(find);
                if (index != -1) {
                    found += 1;
                    missing.splice(index, 1);
                }
            }
        }
        if (missingAtStart != found) {
            return false;
        }

        for (let i = 0; i < wordLength; i++) {
            for (let k = 0; k < wordLength; k++) {
                if (correct[i] == 0 && guess[i] == word[k] && correct[k] != 1) {
                    const letter = word[k];
                    const find = (element) => element['letter'] == letter;
                    const index = missingX.findIndex(find);
                    if (index == -1) {
                        return false;
                    }
                }
            }
        }
    }
    return true;
}

function recursivelyPruneTree(tree, index, guesses, correct) {
    if (index >= wordLength) {
        return doesWordMatchMask(tree, guesses, correct);
    }
    let found = false;
    for (let i = 0; i < letters.length; i++) {
        if (tree[letters[i]]) {
            if (recursivelyPruneTree(tree[letters[i]], index + 1, guesses, correct)) {
                found = true;
            } else {
                delete tree[letters[i]];
            }
        }
    }
    return found;
}

function doesWordMatchGuess(word, guess, correctOne) {
    const correct = correctOne;

    let missing = [];
    for (let i = 0; i < wordLength; i++) {
        if (correct[i] != 1 && guess[i] == word[i]) {
            return false;
        }
        if (correct[i] == 1) {
            if (guess[i] != word[i]) {
                return false;
            }
        } else {
            missing.push({ letter: word[i], position: i });
        }
    }
    let missingAtStart = 0;

    const missingX = [];
    for (let i = 0; i < correct.length; i++) {
        if (correct[i] == 2) {
            missingAtStart += 1;
            missingX.push({ letter: guess[i], position: i });
        }
    }
    let found = 0;
    for (let i = 0; i < wordLength; i++) {
        if (correct[i] != 1) {
            const letter = guess[i];
            const ind = i;
            const find = (element) => element['letter'] == letter && element['position'] != ind;
            const index = missing.findIndex(find);
            if (index != -1) {
                found += 1;
                missing.splice(index, 1);
            }
        }
    }
    if (missingAtStart != found) {
        return false;
    }

    for (let i = 0; i < wordLength; i++) {
        for (let j = 0; j < wordLength; j++) {
            if (correct[i] == 0 && guess[i] == word[j] && correct[j] != 1) {
                const letter = word[j];
                const find = (element) => element['letter'] == letter;
                const index = missingX.findIndex(find);
                if (index == -1) {
                    return false;
                }
            }
        }
    }

    return true;
}

function recursivelyPruneTree2(tree, index, guess, correct) {
    if (index >= wordLength) {
        return doesWordMatchGuess(tree, guess, correct);
    }
    let found = false;
    Object.entries(tree).forEach(([key, value]) => {
        if (recursivelyPruneTree2(value, index + 1, guess, correct)) {
            found = true;
        } else {
            tree[key] = 0;
        }
    });
    return found;
}

function findLargestNotCorrectTreeWithMask(wordTreeLeft, guesses) {
    // this function is too big
    const weightTree = JSON.parse(JSON.stringify(wordTreeLeft));
    let minCount = 0;
    const correct = guesses[guesses.length - 1]['mask'];
    recursivelyPruneTree(weightTree, 0, guesses, correct);
    const count = recursivelyCountTree(weightTree, 0);

    console.log('MinCount ' + minCount);
    if (guesses.length > 0) guesses[guesses.length - 1]['correct'] = correct;
    const guessItem = guesses[guesses.length - 1]['results'];
    const wordcount = recursivelyCountTree(weightTree, 0);
    const hasWord = doesWordTreeHas(weightTree, guesses[guesses.length - 1]['guess']);
    console.log('Words left ' + wordcount + ' tree has guess ' + hasWord);
    guesses[guesses.length - 1]['hasWord'] = hasWord;
    guesses[guesses.length - 1]['wordResults'] = JSON.parse(JSON.stringify(guesses[guesses.length - 1]['results']));
    const wordResults = guesses[guesses.length - 1]['wordResults'];

    for (let i = 0; i < wordLength; i++) {
        if (correct[i] == 1) {
            guessItem[i]['correct'] = true;
        }
        if (correct[i] == 2 && guessItem[i]['correct'] == false) {
            guessItem[i]['inWord'] = true;
        }
        if (correct[i] == 0 && guessItem[i]['inWord'] == false && guessItem[i]['correct'] == false) {
            guessItem[i]['notInWord'] = true;
        }
    }
    return weightTree;
}

function calcMatches(correct, guess) {
    let match = 0;
    let close = 0;
    let miss = wordLength;
    for (let i = 0; i < wordLength; i++) {
        if (correct[i] == 1) {
            match += 1;
            miss -= 1;
        }
        if (correct[i] == 2) {
            close += 1;
            miss -= 1;
        }
    }
    return { match, close, miss };
}

function generateCombination() {
    const helper = '';
    const corrects = [];
    function Generate(helper, limit) {
        if (helper.length == limit) {
            const correct = [];
            for (let i = 0; i < limit; i++) {
                correct.push(parseInt(helper[i]));
            }
            corrects.push(correct);
        } else {
            Generate(helper + 0, limit);
            Generate(helper + 1, limit);
            Generate(helper + 2, limit);
        }
    }

    Generate(helper, wordLength);
    return corrects;
}

function findLargestGreenMask(guesses) {
    const mask = [];
    for (let i = 0; i < wordLength; i++) {
        mask.push(0);
    }

    for (let i = 0; i < guesses.length; i++) {
        const test = guesses[i]['mask'];
        for (let j = 0; j < wordLength; j++) {
            if (test[j] == 1) {
                mask[j] = 1;
            }
        }
    }
    return mask;
}

function findPossibleLettersList(mask, words, guesses) {
    let result = [];
    for (let j = 0; j < wordLength; j++) {
        if (mask[j] == 0) {
            for (let i = 0; i < words.length; i++) {
                const letter = words[i][j];
                let guessHas = false;
                for (let k = 0; k < guesses.length; k++) {
                    const guess = guesses[k]['guess'];
                    if (guess[j] == letter) {
                        guessHas = true;
                    }
                }
                if (!guessHas) {
                    const find = (e) => e == letter;
                    if (result.findIndex(find) == -1) {
                        result.push(letter);
                    }
                }
            }
        }
    }
    return result;
}

function findSureLettersOfWord(guesses) {
    let result = [];
    for (let j = 0; j < wordLength; j++) {
        result.push('0');
        for (let k = 0; k < guesses.length; k++) {
            const guess = guesses[k]['guess'];
            const mask = guesses[k]['correct'];
            if (mask[j] == 1) {
                result[j] = guess[j];
            }
        }
    }
    return result;
}

function findPossibleWordList(mask, words, letterList, guesses, protoWord) {
    let result = [];
    for (let i = 0; i < words.length; i++) {
        const word = words[i];
        let found = false;
        for (let j = 0; j < wordLength; j++) {
            if (mask[j] == 0) {
                const letter = word[j];
                const find = (e) => e == letter && protoWord[j] != word[j];
                if (letterList.findIndex(find) != -1) {
                    found = true;
                }
            }
        }
        if (found) {
            result.push(word);
        }
    }
    for (let i = 0; i < guesses.length; i++) {
        const guess = guesses[i]['guess'];
        const find = (el) => el == guess;
        const index = result.findIndex(find);
        if (index != -1) {
            result.splice(index, 1);
        }
    }
    return result;
}

function isMaskMissingXLetter(mask, number) {
    let count = 0;
    for (let i = 0; i < wordLength; i++) {
        if (mask[i] == 1) count++;
    }
    return number >= wordLength - count;
}

function findSmallestTreeAndWord(wordTreeLeft) {
    // this function is too big
    const weightTreeOriginal = JSON.parse(JSON.stringify(wordTreeLeft));
    let resultTree = JSON.parse(JSON.stringify(weightTreeOriginal));

    let bestGuess;
    let bestCountOverall = 100000;
    const startingCount = bestCountOverall;
    const originalCount = recursivelyCountTree(resultTree, 0);
    // make possible word list ?
    const mask = findLargestGreenMask(guesses);
    const maskIsMissingXLetters = isMaskMissingXLetter(mask, 4);

    const protoWord = findSureLettersOfWord(guesses);
    const letterList = findPossibleLettersList(mask, remainingWords, guesses);
    console.log('Letters left' + JSON.stringify(letterList));
    let possibleWordList = findPossibleWordList(mask, wordList, letterList, guesses, protoWord);

    possibleWordList =
        maskIsMissingXLetters && guesses.length < 5 && remainingWords.length > 2 ? possibleWordList : remainingWords;

    for (let j = 0; j < possibleWordList.length; j++) {
        let minCount = 0;
        let minMatches = { match: wordLength + 1, close: wordLength + 1, miss: -1 };
        const guess = possibleWordList[j];
        const corrects = generateCombination();
        for (let i = 0; i < corrects.length; i++) {
            const correct = corrects[i];
            const matches = calcMatches(correct, guess);
            const weightTree = JSON.parse(JSON.stringify(weightTreeOriginal));
            recursivelyPruneTree2(weightTree, 0, guess, correct);
            const count = recursivelyCountTree(weightTree, 0);

            const countIsGood = count > 0 && count > minCount;
            const countIsSame = count > 0 && count >= minCount;
            const correctMatchesIsBetter = matches['match'] < minMatches['match'];
            const matchesAreBetterOrSame = correctMatchesIsBetter && matches['close'] <= matches['close'];
            const closeMatchesIsBetter = matches['close'] < matches['close'];
            const matchesAreBetter =
                matchesAreBetterOrSame && (correctMatchesIsBetter || (correctMatchesIsBetter && closeMatchesIsBetter));
            const notCorrectMask = !(
                correct[0] == 1 &&
                correct[1] == 1 &&
                correct[2] == 1 &&
                correct[3] == 1 &&
                correct[4] == 1
            );
            const notFalseMask = !(
                correct[0] == 0 &&
                correct[1] == 0 &&
                correct[2] == 0 &&
                correct[3] == 0 &&
                correct[4] == 0
            );
            if (countIsGood || (countIsSame && matchesAreBetter)) {
                if (notCorrectMask || count == 1 || (notFalseMask && count < originalCount)) {
                    console.log(
                        'Count ' + count + ' match ' + matches['match'] + ' close ' + matches['close'] + ' ' + correct
                    );
                    minCount = count;
                    resultTree = JSON.parse(JSON.stringify(weightTree));
                    minMatches = JSON.parse(JSON.stringify(matches));
                }
            }
        }
        if (bestCountOverall > minCount) {
            bestCountOverall = minCount;
            bestGuess = guess;
        }
    }
    console.log('best count ' + bestCountOverall);
    return bestGuess;
}

function pruneWordTreeWithMask(guesses) {
    wordTreeLeft = findLargestNotCorrectTreeWithMask(wordTreeLeft, guesses);
    if (wordTreeLeft) console.log(JSON.stringify(wordTreeLeft));
    updateTree(scene, wordTreeLeft);
    return wordTreeLeft;
}

function makeGuesses() {
    const result = findSmallestTreeAndWord(wordTreeLeft);
    return result;
}

function makeGuessesWithMask(guesses) {
    pruneWordTreeWithMask(guesses);
}

function parseMaskString(guessMask) {
    let mask = [];
    for (let i = 0; i < wordLength; i++) {
        mask.push(parseInt(guessMask[i]));
    }
    return mask;
}

function recursiveGetWordListFromTree(tree, index, list) {
    let sum = 0;
    if (index >= wordLength) {
        list.push(tree);
        return 1;
    }
    for (let i = 0; i < letters.length; i++) {
        const letter = letters[i];
        if (tree[letter]) {
            sum += recursiveGetWordListFromTree(tree[letter], index + 1, list);
        }
    }
    return sum;
}

function updateWordsLeft() {
    const list = [];
    const count = recursiveGetWordListFromTree(wordTreeLeft, 0, list);

    list.sort((a, b) => {
        for (let i = 0; i < wordLength; i++) {
            if (a[i] < b[i]) return 1;
            if (b[i] < a[i]) return -1;
        }
        return 0;
    });
    remainingWords = list;
    document.getElementById('remainingWords').innerHTML = 'Words left ' + count + '\n' + list.join(',\n');
}

function findFirstWordWithMissingLetters(tree, missingLetters, index) {
    if (index >= wordLength) {
        return tree;
    }
    let word = '';
    for (let i = 0; i < letters.length; i++) {
        const letter = letters[i];
        if (tree[letter]) {
            if (missingLetters[letter]) {
                const result = findFirstWordWithMissingLetters(tree[letter], missingLetters[letter], index + 1);
                if (result != '') {
                    return result;
                }
            }
        }
    }
    return word;
}

function findFirstWordWithMissingLetters2(tree, index, addedLetters, guesses, missingLetterList) {
    if (index >= wordLength) {
        for (let i = 0; i < guesses.length; i++) {
            if (guesses[i]['guess'] == tree) {
                return false;
            }
        }
        return tree;
    }
    let word = '';
    for (let i = 0; i < letters.length; i++) {
        const letter = letters[i];
        if (tree[letter]) {
            const find = (elem) => elem == letter;
            if (addedLetters.findIndex(find) == -1 && missingLetterList.findIndex(find) != -1) {
                const result = findFirstWordWithMissingLetters2(
                    tree[letter],
                    index + 1,
                    addedLetters.concat([letter]),
                    guesses,
                    missingLetterList
                );
                if (result != '') {
                    return result;
                }
            }
        }
    }
    return word;
}

function checkCorrectMaskForLetter(letter, index) {
    for (let i = 0; i < guesses.length; i++) {
        const mask = guesses[i]['correct'];
        const guess = guesses[i]['guess'];
        if (mask[index] == 1 && guess[index] == letter) {
            return false;
        }
    }
    return true;
}

function recursiveFindMostLetterLeftInTree(tree, missingLetters, index) {
    if (index >= wordLength) {
        return { word: tree, count: missingLetters.length };
    }
    let result = { word: '', count: 0 };
    for (let i = 0; i < letters.length; i++) {
        const letter = letters[i];
        if (tree[letter]) {
            const missingLettersNew = [];
            if (checkCorrectMaskForLetter(letter, index)) {
                const find = (element) => element == letter;
                if (missingLetters.findIndex(find) == -1) {
                    missingLetters.push(letter);
                }
            }
            const midResult = recursiveFindMostLetterLeftInTree(
                tree[letter],
                missingLetters.concat(missingLettersNew),
                index + 1
            );
            if (midResult['count'] > result['count']) {
                result = midResult;
            }
        }
    }
    return result;
}

function findMostLettersLeftInTree(tree) {
    const missingLetters = [];
    const result = recursiveFindMostLetterLeftInTree(tree, missingLetters, 0);
    return result['word'];
}

function addWordMissingLettersInIndex(letter, index, missingLetters, guesses, missingLetterList) {
    for (let i = 0; i < guesses.length; i++) {
        let guess = guesses[i]['guess'];
        if (guess[index] == letter) {
            return false;
        }
    }
    missingLetters[letter] = {};
    return true;
}

function recursiveFindLettersLeft(tree, index, missingLetters, guesses, missingLetterList) {
    if (index >= wordLength) {
        for (let i = 0; i < wordLength; i++) {
            const letter = tree[i];
            const find = (elem) => elem == letter;
            if (missingLetterList.findIndex(find) == -1) {
                missingLetterList.push(letter);
            }
        }
        return;
    }
    for (let i = 0; i < letters.length; i++) {
        const letter = letters[i];
        if (tree[letter]) {
            if (addWordMissingLettersInIndex(letter, index, missingLetters, guesses, missingLetterList)) {
                recursiveFindLettersLeft(tree[letter], index + 1, missingLetters[letter], guesses, missingLetterList);
            }
        }
    }
}

function findBestGuess() {
    if (remainingWords.length == 1) {
        guessElem.value = remainingWords[0];
        document.getElementById('bestGuesses').innerHTML = remainingWords[0];
        return;
    }

    if (
        guesses.length == 1 &&
        guesses[0]['guess'] == startingPairs[startingPair][0] &&
        wordLength == 5 &&
        (wordListName == 'sanuli 5' || wordListName == 'suomi5')
    ) {
        const bestWord = startingPairs[startingPair][1];
        guessElem.value = bestWord;
        document.getElementById('bestGuesses').innerHTML = bestWord;
        return;
    }

    if (guesses.length > 2) {
        let bestGuess = makeGuesses();
        if (bestGuess != '') {
            guessElem.value = bestGuess;
            document.getElementById('bestGuesses').innerHTML = bestGuess;
            console.log('success');
            return;
        }
    }
    console.log('failed');
    const missingLetters = {};
    const missingLetterList = [];
    recursiveFindLettersLeft(wordTreeLeft, 0, missingLetters, guesses, missingLetterList);
    let bestWord = findFirstWordWithMissingLetters2(wordTree, 0, [], guesses, missingLetterList);
    if (bestWord == '') {
        console.log('trying other method');
        bestWord = findFirstWordWithMissingLetters(wordTree, missingLetters, 0);
    }
    if (bestWord == '') {
        console.log(JSON.stringify(missingLetters));
        bestWord = findMostLettersLeftInTree(wordTreeLeft);
    }
    guessElem.value = bestWord;
    document.getElementById('bestGuesses').innerHTML = bestWord;
}

function makeGuess() {
    if (won) {
        newWord();
    }
    if (guesses.length >= 6) {
        setStatus('Hävisit jo pelin. Aloita uusi');
        guessElem.value = '';
        return;
    }

    let guessMask = guessMaskElem.value;
    if (guessMask[wordLength - 1] === '\n') {
        guessMask.splice(wordLength - 1, 1);
    }
    if (guessMask.length != wordLength) {
        setStatus('Maskin on oltava ' + wordLength + ' kirjainta');
        guessMaskElem.value = '';
        return;
    }

    let broken = false;
    for (let j = 0; j < guessMask.length; j++) {
        const letter = guessMask[j];
        if ('012'.search(letter) == -1) {
            broken = true;
            break;
        }
    }
    if (broken) {
        setStatus('Maskin on sisällettävä vain kirjaimia 0,1,2');
        guessMaskElem.value = '';
        return;
    }

    let guess = guessElem.value.toUpperCase();
    if (guess[wordLength - 1] === '\n') {
        guess.splice(wordLength - 1, 1);
    }

    if (guess.length != wordLength) {
        setStatus('Sanan on oltava ' + wordLength + ' kirjainta');
        guessElem.value = '';
        return;
    }

    if (!checkWordForLegalCharacters(guess)) {
        setStatus('Sanassa on laittomia merkkejä');
        guessElem.value = '';
        return;
    }

    if (doesWordTreeHas(originalWordTree, guess)) {
        console.log('Made guess ' + guess);
        setStatus('Arvasit ' + guess);
        const results = [];
        for (let i = 0; i < wordLength; i++) {
            results.push({ letter: guess[i], correct: false, inWord: false, notInWord: true });
        }
        const wordResults = JSON.parse(JSON.stringify(results));
        guesses.push({ guess: guess, results: results, wordResults: wordResults, mask: parseMaskString(guessMask) });
        guessesLeft.innerHTML = 'Sinulla on ' + (6 - guesses.length) + ' arvausta jäljellä.';

        makeGuessesWithMask(guesses);
        updateGuessesList();
        updateWordsLeft();
        findBestGuess();
    } else {
        setStatus('Peli ei tunne sanaa ' + guess);
        guessElem.value = '';
    }
}

function removeFromTree(tree, word, index) {
    if (tree === undefined) return false;
    if (tree == word) return true;
    if (removeFromTree(tree[word[index]], word, index + 1)) {
        delete tree[word[index]];
        for (let j = 0; j < letters.length; j++) {
            if (tree[letters[j]]) {
                return false;
            }
        }
        return true;
    }
    return false;
}

function removeWord() {
    const word = guessElem.value;
    removeFromTree(wordTree, word, 0);
    removeFromTree(wordTreeLeft, word, 0);
    const find = (el) => el == word;
    let index = 0;
    while (index != -1) {
        index = wordList.findIndex(find);
        wordList.splice(index, 1);
    }
    while (index != -1) {
        index = wordList.findIndex(find);
        remainingWords.splice(index, 1);
    }
    findBestGuess();
    updateWordsLeft();
}

function init() {
    document.getElementById('newWord').onclick = () => {
        newWord();
    };

    document.getElementById('remove').onclick = () => {
        removeWord();
    };
    document.getElementById('guess').onclick = makeGuess;
    statusElem = document.getElementById('status');
    guessElem = document.getElementById('guessText');
    guessMaskElem = document.getElementById('guessMask');
    guessesDiv = document.getElementById('guesses');
    guessesLeft = document.getElementById('guessesLeft');
    const visualisationCheckBox = document.getElementById('visualisationCheckBox');
    visualisationCheckBox.checked = visualisation;
    visualisationCheckBox.onclick = () => {
        visualisation = visualisationCheckBox.checked;
        saveLocalStorage();
        updateTree(scene, wordTreeLeft);
    };

    guessElem.onkeypress = function (e) {
        if (!e) e = window.event;
        var keyCode = e.code || e.key;
        if (keyCode == 'Enter') {
            makeGuess();
            return false;
        }
    };

    guessMaskElem.onkeypress = function (e) {
        if (!e) e = window.event;
        var keyCode = e.code || e.key;
        if (keyCode == 'Enter') {
            makeGuess();
            return false;
        }
    };
    const wordListSelect = document.getElementById('wordListSelect');
    for (let i = 0; i < wordListNames.length; i++) {
        const name = wordListNames[i];
        const option = document.createElement('option');
        if (name == wordListName) {
            option.selected = true;
        }
        option.value = name;
        option.text = name;
        wordListSelect.appendChild(option);
    }
    wordListSelect.onchange = (x, y) => {
        wordListName = wordListSelect.value;
        saveLocalStorage();
        newWord();
    };
}

function loadLocalStorage() {
    const value = localStorage.getItem('Snuffli');
    if (value) {
        localValues = JSON.parse(value);
        if (localValues.version !== currentVersion) {
            localValues = { wordLists: {}, wordListNames: [] };
            return;
        }
        for (let i = 0; i < localValues['wordListNames'].length; i++) {
            const name = localValues['wordListNames'][i];
            if (!words[name]) {
                words[name] = {};
            }
            words[name]['exclusion'] = localValues['wordLists'][name]['exclusion'];
        }
        if (localValues.wordListName) {
            wordListName = localValues.wordListName;
        }
        if (localValues.visualisation) {
            visualisation = localValues.visualisation;
        }
    } else {
        localValues = { wordLists: {}, wordListNames: [] };
    }
}

function saveLocalStorage() {
    for (let i = 0; i < wordListNames.length; i++) {
        const name = wordListNames[i];
        localValues['wordListNames'] = wordListNames;
        if (!localValues['wordLists']) {
            localValues['wordLists'] = {};
        }
        if (!localValues['wordLists'][name]) {
            localValues['wordLists'][name] = {};
        }
        localValues['wordLists'][name]['exclusion'] = words[name]['exclusion'];
    }
    localValues.wordListName = wordListName;
    localValues.visualisation = visualisation;
    localValues.version = currentVersion;
    localStorage.setItem('Snuffli', JSON.stringify(localValues));
}

function loadWordFile(info) {
    return new Promise(function (resolve, reject) {
        fetch(info['filename'])
            .then((response) => response.text())
            .then((text) => {
                const wordList = text.toUpperCase().split('\n');
                console.log(info['name']);
                const originalWordTree = makeWordTree(wordList, info['wordLength']);
                const wordTree = JSON.parse(JSON.stringify(originalWordTree));
                const exclusion =
                    words[info['name']] && words[info['name']]['exlusion'] ? words[info['name']]['exclusion'] : [];
                for (let i = 0; i < exclusion.length; i++) {
                    const word = exclusion[i];
                    const test = (element) => element == word;
                    const index = wordList.findIndex(test);
                    if (index != -1) {
                        wordList.splice(index, 1);
                    }
                }
                wordListNames.push(info['name']);
                words[info.name] = { wordList, wordTree, originalWordTree, info, exclusion };
            })
            .then(() => {
                resolve();
            });
    });
}

function loadWords() {
    const promises = [
        { name: 'suomi 3', language: 'finnish', wordLength: 3, filename: './words/kotus3letterlist.txt' },
        { name: 'suomi 4', language: 'finnish', wordLength: 4, filename: './words/kotus4letterlist.txt' },
        { name: 'suomi 5', language: 'finnish', wordLength: 5, filename: './words/kotus5letterlist.txt' },
        { name: 'suomi 6', language: 'finnish', wordLength: 6, filename: './words/kotus6letterlist.txt' },
        { name: 'suomi 7', language: 'finnish', wordLength: 7, filename: './words/kotus7letterlist.txt' },
        { name: 'sanuli 5', language: 'finnish', wordLength: 5, filename: './words/sanuli5letterlist.txt' },
        { name: 'englanti 5 10k', language: 'english', wordLength: 5, filename: './words/10kenglish5letterlist.txt' },
        { name: 'englanti 6 10k', language: 'english', wordLength: 6, filename: './words/10kenglish6letterlist.txt' },
        { name: 'englanti 5 20k', language: 'english', wordLength: 5, filename: './words/20kenglish5letterlist.txt' },
        { name: 'englanti 6 20k', language: 'english', wordLength: 6, filename: './words/20kenglish6letterlist.txt' },
        { name: 'englanti 5 30k', language: 'english', wordLength: 5, filename: './words/30kenglish5letterlist.txt' },
        { name: 'englanti 6 30k', language: 'english', wordLength: 6, filename: './words/30kenglish6letterlist.txt' },
    ].map(loadWordFile);

    loadLocalStorage();
    const loader = new FontLoader();
    loader.load('./js/arial.json', function (font) {
        makevis();
        for (let i = 0; i < letters.length; i++) {
            const letter = letters[i];
            const geometry = new TextGeometry(letter, {
                font: font,
                size: 0.3,
                height: 0.01,
                curveSegments: 3,
                bevelEnabled: true,
                bevelThickness: 0.001,
                bevelSize: 0.001,
                bevelOffset: 0,
                bevelSegments: 2,
            });
            const textMaterial = new THREE.MeshPhongMaterial({ color: 0x228822 });
            const textMaterial2 = new THREE.MeshPhongMaterial({ color: 0x444422 });
            const mesh = new THREE.Mesh(geometry, textMaterial);
            const mesh2 = new THREE.Mesh(geometry, textMaterial2);
            mesh.position.set(-0.15, 0.5, 0);
            mesh2.position.set(-0.15, 0.5, 0);
            letterMeshes.push(mesh);
            letterMeshes2.push(mesh2);
        }

        Promise.all(promises)
            .then(() => {
                init();
            })
            .then(() => {
                fetch('./words/kotuspairs5.txt')
                    .then((response) => response.text())
                    .then((text) => {
                        startingPairs = [];
                        const pairStrings = text.toUpperCase().split('\n');
                        for (let i = 0; i < pairStrings.length; i++) {
                            const pair = pairStrings[i].split(',');
                            startingPairs.push(pair);
                        }
                    })
                    .then(() => {
                        newWord();
                    });
            });
    });
}

function onMouseDown(ev) {
    if (trunk) {
        originalRotation = trunk.rotation.y;
        originalX = ev.clientX;
        step = 0;
    }
}

function onMouseMove(ev) {
    if (trunk && ev.buttons == 1) {
        const diff = originalX - ev.clientX;
        rotationChange = (Math.PI * 2.5 * -diff) / window.innerWidth;
    }
}

function onMouseUp(ev) {
    if (trunk) {
        originalRotation = trunk.rotation.y;
        step = 0.01;
    }
}

function makevis() {
    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 500);

    document.body.onresize = (ev) => {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize(window.innerWidth, window.innerHeight);
    };

    renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    const vis = document.getElementById('visualisation');
    vis.appendChild(renderer.domElement);
    vis.onmousedown = onMouseDown;
    vis.onmousemove = onMouseMove;
    vis.onmouseup = onMouseUp;

    const light = new THREE.AmbientLight(0x404040); // soft white light
    scene.add(light);
    const directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);
    scene.add(directionalLight);

    camera.position.set(0, 0, 5.0);
    camera.lookAt(0, 0, 0);

    function animate() {
        if (trunk) {
            if (step == 0) {
                trunk.rotation.y = originalRotation + rotationChange;
            }
            trunk.rotation.y += step;
        }
        requestAnimationFrame(animate);
        renderer.render(scene, camera);
    }
    animate();
}

function updateTree(scene, wordTree) {
    if (trunk != null) {
        console.log('remove tree');
        scene.remove(trunk);
    }
    if (!visualisation) return;

    const lineMaterial = new THREE.MeshPhongMaterial({ color: 0x887755 });
    const trunkGeometry = new THREE.ConeGeometry(/*0.025,*/ 0.04, 1.5, 11, 1, 0);
    trunkGeometry.applyMatrix4(new THREE.Matrix4().makeTranslation(0, 0.75, 0));
    trunk = new THREE.Mesh(trunkGeometry, lineMaterial);
    const mainRoot = trunk.clone();
    mainRoot.rotateX(Math.PI);
    trunk.add(mainRoot);
    scene.add(trunk);
    const branchGeometry = new THREE.ConeGeometry(/*0.01875,*/ 0.02, 1.0, 7, 1, 0);
    branchGeometry.applyMatrix4(new THREE.Matrix4().makeTranslation(0, 0.5, 0));
    const scaler = 0.8;

    const trunkColor = new THREE.Color(0x334422);
    const trunkColor2 = new THREE.Color(0x887755);
    const leafColor = new THREE.Color(0x227722);
    const rootColor = new THREE.Color(0x223311);
    const branches = [];
    const roots = [];
    for (let i = 0; i < wordLength; i++) {
        const branchMaterial = new THREE.MeshPhongMaterial({
            color: new THREE.Color().lerpColors(trunkColor, leafColor, i / 5.0),
        });
        const branch = new THREE.Mesh(branchGeometry, branchMaterial);
        branches.push(branch);
        const rootMaterial = new THREE.MeshPhongMaterial({
            color: new THREE.Color().lerpColors(trunkColor2, rootColor, i / 5.0),
        });
        const root = new THREE.Mesh(branchGeometry, rootMaterial);
        roots.push(root);
    }

    function recTree(treeRoot, mainbranch, depth, root) {
        const vowels = 'EYUIOÅAÖÄ';
        let vowelCount = 0;
        let consonantCount = 0;
        if (depth >= wordLength) return;
        for (let i = 0; i < letters.length; i++) {
            const letter = letters[i];
            if (treeRoot[letter]) {
                const vowel = vowels.search(letter) != -1;
                const first = depth == 0;
                const firstVowel = vowel && first;
                root = first ? firstVowel : root;
                const branch = root ? roots[depth].clone() : branches[depth].clone();
                branch.position.set(0, (firstVowel ? -1 : 1) * (0.6 - Math.random() * 0.4), 0);
                branch.scale.set(scaler, scaler, scaler);
                if (firstVowel) branch.rotateX(Math.PI);

                if (first) {
                    vowelCount += vowel ? 1 : 0;
                    consonantCount += vowel ? 0 : 1;
                    if (vowel) {
                        branch.rotateY((vowelCount ** Math.PI * 2.0) / vowels.length);
                    } else {
                        branch.rotateY((consonantCount * Math.PI * 2.0) / (letters.length - vowels.length));
                    }
                } else {
                    branch.rotateY(Math.random() * 0.1 + (i * Math.PI * 2.0) / letters.length);
                }
                branch.rotateX(Math.random() * 0.6 + 0.5 + (root ? depth * -0.4 : 0.0));
                branch.add(root ? letterMeshes2[i].clone() : letterMeshes[i].clone());
                mainbranch.add(branch);
                recTree(treeRoot[letters[i]], branch, depth + 1, root);
            }
        }
    }
    recTree(wordTree, trunk, 0, true);
}

loadWords();
